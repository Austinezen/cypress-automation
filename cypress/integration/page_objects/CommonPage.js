
export default class CommonPage {

    setViewport(viewportSize) {
        switch (viewportSize) {
            case 'desktop': {
                cy.viewport(1920, 1080)
                break;
            }
            default: {
                throw new Error("vieport size was not recognized please check your feature files")
            }
        }

    }

    visitSite(url) {
        cy.viewport(1920, 1080)
        cy.visit(url)
    }

    setCookies() {
        cy.get('button').contains('Zustimmen').click()
    }

    checkUrl(url) {
        cy.url().should('include', url)
    }

    click1() {
        cy.get('#flyout2').click()
    }

    click2() {
        cy.get('a.navigation__link')
            .contains('Private')
            .click({ force: true })
    }
}

