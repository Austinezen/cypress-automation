//const URL = 'https://www.huk.de/'
const FORCE = '{force: true}'

export default class LandingPage {

    viewport() {
        cy.viewport(1920, 1080)
    }

    visit(url) {
        cy.viewport(1920, 1080)
        cy.visit(url)
    }

    acceptCookies() {
        cy.get('button').contains('Zustimmen').click()
    }

    checkurl() {
        cy.url().should('include', 'www.huk.de')
    }

    click1() {
        cy.get('#flyout2').click()
    }

    click2() {
        cy.get('a.navigation__link')
            .contains('Private')
            .click({ force: true })
    }
}

