export default class PrivateHaftpflichtVersicherungsPage {

    click3() {
        cy.get('a')
            .contains('Jetzt berechnen')
            .click();
    }

    select() {
        cy.get('.form-select-button__button')
            .contains('Sie selbst als Single')
            .click();
    }
}

