import cypress from 'cypress'
import { Given, Then, When, } from 'cypress-cucumber-preprocessor/steps'

Given('I change the viewport to desktop', () => {
    cy.viewport(1920, 1080)
    cy.visit('https://www.huk.de/')
})